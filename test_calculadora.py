from unittest import TestCase

from Calculadora import calculadora

class TestCalculadora(TestCase):
    def test_calcularestaditica(self):
        self.assertEqual(calculadora().calcularestadistica(""),[0, 0, 0, 0],"Cadena Vacia")

    def test_calcularestaditicaConUnNumero(self):
        self.assertEqual(calculadora().calcularestadistica("1"),[1, 1, 1, 1],"Cadena con un numero")

    def test_calcularestaditicaConDosNumeros(self):
        self.assertEqual(calculadora().calcularestadistica("1, 9"),[2, 1, 9, 5],"Cadena con dos numeros")

    def test_calcularestaditicaConNNumeros(self):
        self.assertEqual(calculadora().calcularestadistica("6, 2, 4, 5, 8"),[5, 2, 8, 5],"Cadena con n numeros")