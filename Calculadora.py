class calculadora:
    def calcularestadistica(self, cadena):
        if cadena == "":
            return [0, 0, 0, 0]
        elif "," in cadena:
            numeros = cadena.split(",")
            numeros = list (map(int, numeros))
            return [len(numeros), min(numeros), max(numeros), sum(numeros)/len(numeros)]
        else:
            return [1, int(cadena[0]), int(cadena[0]), int(cadena[0])]